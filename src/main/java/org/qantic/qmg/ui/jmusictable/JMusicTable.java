/*
	This file is part of qantic-media-geek.

    qantic-media-geek is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    qantic-media-geek is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with qantic-media-geek.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.qantic.qmg.ui.jmusictable;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

public class JMusicTable extends JTable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7618969188088222287L;

	public JMusicTable() {

		super();
	}

	public JMusicTable(AbstractTableModel tmodel) {

		super(tmodel);

		this.setAutoCreateRowSorter(true);
	}
}
