/*
	This file is part of qantic-media-geek.

    qantic-media-geek is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    qantic-media-geek is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with qantic-media-geek.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.qantic.qmg.ui.jmusictable;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import javax.swing.filechooser.FileSystemView;
import javax.swing.table.AbstractTableModel;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.AudioHeader;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;

import org.qantic.ui.swing.jfstree.IFsTreeTableModel;

public class MusicTableModel extends AbstractTableModel implements IFsTreeTableModel{

	private static final Logger LOGGER = Logger.getGlobal();

	private static final long serialVersionUID = -154074727679183032L;

	private final FileSystemView fsv = FileSystemView.getFileSystemView();
	private final String[] musicColumnModel = { "File Name", "Artist", "Album", "Title", "Year", "Genre", "Track #", "Bitrate", "Time" };
	private String[] filterExtensionList = { "mp3", "flac", "wma"};
	private String[][] tag_cache;
	private File[] model_files = null;

	public MusicTableModel() {
		this(new File[0]);
	}

	public MusicTableModel(File[] files) {
		this.model_files = files;
	}

	@Override
	public int getRowCount() {
		return this.model_files.length;
	}

	@Override
	public int getColumnCount() {
		return this.musicColumnModel.length;
	}

	@Override
	public String getColumnName(int column) {
		return this.musicColumnModel[column];
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		return this.tag_cache[rowIndex][columnIndex];
	}

	@Override
	public String[] getExtensionList() {

		return filterExtensionList;
	}

	@Override
	public void setFileList(File[] files) {

		this.model_files = files == null ? new File[0] : files;

		this.tag_cache = new String[this.model_files.length][9];

		for (int i = 0; i < this.model_files.length; i++) {

			AudioFile audioFile;
			Tag audioTag;
			AudioHeader audioheader;
			try {
				audioFile = AudioFileIO.read(this.model_files[i]);
				audioTag = audioFile.getTag();
				audioheader = audioFile.getAudioHeader();

				this.tag_cache[i][0] = this.fsv.getSystemDisplayName(this.model_files[i]);
				this.tag_cache[i][1] = audioTag == null ? "" : audioTag.getFirst(FieldKey.ARTIST);
				this.tag_cache[i][2] = audioTag == null ? "" : audioTag.getFirst(FieldKey.ALBUM);
				this.tag_cache[i][3] = audioTag == null ? "" : audioTag.getFirst(FieldKey.TITLE);
				this.tag_cache[i][4] = audioTag == null ? "" : audioTag.getFirst(FieldKey.YEAR);
				this.tag_cache[i][5] = audioTag == null ? "" : audioTag.getFirst(FieldKey.GENRE);
				this.tag_cache[i][6] = audioTag == null ? "" : audioTag.getFirst(FieldKey.TRACK);
				this.tag_cache[i][7] = audioheader.getBitRate();
				int s = audioheader.getTrackLength();
				if (s >= 3600) {
					this.tag_cache[i][8] = String.format("%d:%02d:%02d", s / 3600, (s % 3600) / 60, (s % 60));
				}
				this.tag_cache[i][8] = String.format("%d:%02d", s / 60, (s % 60));

			} catch (CannotReadException e) {
				LOGGER.warning(e.getMessage());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TagException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ReadOnlyFileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidAudioFrameException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		this.fireTableDataChanged();
	}

	public void setExtensionList(String[] extList) {

		filterExtensionList = extList;
	}

	public File getFileByRow(int rowIndex) {
		return this.model_files[rowIndex];
	}
}
