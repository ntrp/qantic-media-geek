/*
	This file is part of qantic-media-geek.

    qantic-media-geek is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    qantic-media-geek is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with qantic-media-geek.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.qantic.qmg.ui;

import java.awt.EventQueue;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import net.miginfocom.swing.MigLayout;

import org.qantic.qmg.ui.jmusictable.JMusicTable;
import org.qantic.qmg.ui.jmusictable.MusicTableModel;
import org.qantic.ui.swing.jfstree.JFsTree;

public class QmgGui {

	// private final static Logger LOGGER = Logger.getLogger("GLOBAL_LOGGER");

	private final String appTitle;
	private final int FRAME_WIDTH = 1300;
	private final int FRAME_HEIGHT = 700;

	private JFrame mainFrame;
	private JPanel mainPanel;
	private JFsTree fsTree;
	private JMusicTable mTable;

	public QmgGui(String appt) {

		appTitle = appt;
	}

	public void build() throws InvocationTargetException, InterruptedException {

		if (EventQueue.isDispatchThread()) {

			init();
		} else {
			EventQueue.invokeAndWait(new Runnable() {

				@Override
				public void run() {

					init();
				}
			});
		}
	}

	private void init() {

		mainFrame = new JFrame(appTitle);
		mainPanel = new JPanel();
		mainFrame.setContentPane(mainPanel);
		configureDefaults();
		initComponents();
		composeGui();
	}

	private void configureDefaults() {

		mainFrame.setLocation(0, 0);
		mainFrame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// mainFrame.setMinimumSize(new Dimension(915, 600));
		mainFrame.setResizable(true);
		mainFrame.setVisible(true);
	}

	private void initComponents() {

		fsTree = new JFsTree();
		mTable = new JMusicTable(new MusicTableModel());
		fsTree.setDetailTable(mTable);
	}

	private void composeGui() {

		mainPanel.setLayout(new MigLayout("", "[200][grow]", "[grow]"));
		mainPanel.add(new JScrollPane(fsTree), "grow");
		mainPanel.add(new JScrollPane(mTable), "grow");
	}
}
