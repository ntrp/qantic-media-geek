/*
	This file is part of qantic-media-geek.

    qantic-media-geek is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    qantic-media-geek is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with qantic-media-geek.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.qantic.qmg;

import java.awt.EventQueue;
import java.lang.reflect.InvocationTargetException;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.qantic.qmg.ui.QmgGui;

import com.alee.laf.WebLookAndFeel;

public class QanticMediaGeek {

	public final static String APP_TITLE = "Qantic Media Geek";

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {

				try {
					//UIManager.setLookAndFeel(new SyntheticaSimple2DLookAndFeel());
					//UIManager.setLookAndFeel(new Plastic3DLookAndFeel());
					UIManager.setLookAndFeel ( new WebLookAndFeel ());

					QmgGui mainWindow = new QmgGui(APP_TITLE);
					mainWindow.build();

				} catch (InvocationTargetException | InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnsupportedLookAndFeelException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
}
